# TODO

- [ ] Add multi-line support (functions, for, while, etc.)
- [ ] REPL-like usage (in python's REPL you can use variables and such)
- [ ] Add more shell-like functionality (parse PATH, aliases, etc.)