# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.25

# Default target executed when no arguments are given to make.
default_target: all
.PHONY : default_target

# Allow only one "make -f Makefile2" at a time, but pass parallelism.
.NOTPARALLEL:

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Disable VCS-based implicit rules.
% : %,v

# Disable VCS-based implicit rules.
% : RCS/%

# Disable VCS-based implicit rules.
% : RCS/%,v

# Disable VCS-based implicit rules.
% : SCCS/s.%

# Disable VCS-based implicit rules.
% : s.%

.SUFFIXES: .hpux_make_needs_suffix_list

# Command-line flag to silence nested $(MAKE).
$(VERBOSE)MAKESILENT = -s

#Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E rm -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/csfore/workspaces/c/ncsh

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/csfore/workspaces/c/ncsh

#=============================================================================
# Targets provided globally by CMake.

# Special rule for the target edit_cache
edit_cache:
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --cyan "Running CMake cache editor..."
	/usr/bin/ccmake -S$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR)
.PHONY : edit_cache

# Special rule for the target edit_cache
edit_cache/fast: edit_cache
.PHONY : edit_cache/fast

# Special rule for the target rebuild_cache
rebuild_cache:
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --cyan "Running CMake to regenerate build system..."
	/usr/bin/cmake --regenerate-during-build -S$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR)
.PHONY : rebuild_cache

# Special rule for the target rebuild_cache
rebuild_cache/fast: rebuild_cache
.PHONY : rebuild_cache/fast

# The main all target
all: cmake_check_build_system
	$(CMAKE_COMMAND) -E cmake_progress_start /home/csfore/workspaces/c/ncsh/CMakeFiles /home/csfore/workspaces/c/ncsh//CMakeFiles/progress.marks
	$(MAKE) $(MAKESILENT) -f CMakeFiles/Makefile2 all
	$(CMAKE_COMMAND) -E cmake_progress_start /home/csfore/workspaces/c/ncsh/CMakeFiles 0
.PHONY : all

# The main clean target
clean:
	$(MAKE) $(MAKESILENT) -f CMakeFiles/Makefile2 clean
.PHONY : clean

# The main clean target
clean/fast: clean
.PHONY : clean/fast

# Prepare targets for installation.
preinstall: all
	$(MAKE) $(MAKESILENT) -f CMakeFiles/Makefile2 preinstall
.PHONY : preinstall

# Prepare targets for installation.
preinstall/fast:
	$(MAKE) $(MAKESILENT) -f CMakeFiles/Makefile2 preinstall
.PHONY : preinstall/fast

# clear depends
depend:
	$(CMAKE_COMMAND) -S$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR) --check-build-system CMakeFiles/Makefile.cmake 1
.PHONY : depend

#=============================================================================
# Target rules for targets named ncsh

# Build rule for target.
ncsh: cmake_check_build_system
	$(MAKE) $(MAKESILENT) -f CMakeFiles/Makefile2 ncsh
.PHONY : ncsh

# fast build rule for target.
ncsh/fast:
	$(MAKE) $(MAKESILENT) -f CMakeFiles/ncsh.dir/build.make CMakeFiles/ncsh.dir/build
.PHONY : ncsh/fast

src/cmds.o: src/cmds.c.o
.PHONY : src/cmds.o

# target to build an object file
src/cmds.c.o:
	$(MAKE) $(MAKESILENT) -f CMakeFiles/ncsh.dir/build.make CMakeFiles/ncsh.dir/src/cmds.c.o
.PHONY : src/cmds.c.o

src/cmds.i: src/cmds.c.i
.PHONY : src/cmds.i

# target to preprocess a source file
src/cmds.c.i:
	$(MAKE) $(MAKESILENT) -f CMakeFiles/ncsh.dir/build.make CMakeFiles/ncsh.dir/src/cmds.c.i
.PHONY : src/cmds.c.i

src/cmds.s: src/cmds.c.s
.PHONY : src/cmds.s

# target to generate assembly for a file
src/cmds.c.s:
	$(MAKE) $(MAKESILENT) -f CMakeFiles/ncsh.dir/build.make CMakeFiles/ncsh.dir/src/cmds.c.s
.PHONY : src/cmds.c.s

src/headers.o: src/headers.c.o
.PHONY : src/headers.o

# target to build an object file
src/headers.c.o:
	$(MAKE) $(MAKESILENT) -f CMakeFiles/ncsh.dir/build.make CMakeFiles/ncsh.dir/src/headers.c.o
.PHONY : src/headers.c.o

src/headers.i: src/headers.c.i
.PHONY : src/headers.i

# target to preprocess a source file
src/headers.c.i:
	$(MAKE) $(MAKESILENT) -f CMakeFiles/ncsh.dir/build.make CMakeFiles/ncsh.dir/src/headers.c.i
.PHONY : src/headers.c.i

src/headers.s: src/headers.c.s
.PHONY : src/headers.s

# target to generate assembly for a file
src/headers.c.s:
	$(MAKE) $(MAKESILENT) -f CMakeFiles/ncsh.dir/build.make CMakeFiles/ncsh.dir/src/headers.c.s
.PHONY : src/headers.c.s

src/helpers.o: src/helpers.c.o
.PHONY : src/helpers.o

# target to build an object file
src/helpers.c.o:
	$(MAKE) $(MAKESILENT) -f CMakeFiles/ncsh.dir/build.make CMakeFiles/ncsh.dir/src/helpers.c.o
.PHONY : src/helpers.c.o

src/helpers.i: src/helpers.c.i
.PHONY : src/helpers.i

# target to preprocess a source file
src/helpers.c.i:
	$(MAKE) $(MAKESILENT) -f CMakeFiles/ncsh.dir/build.make CMakeFiles/ncsh.dir/src/helpers.c.i
.PHONY : src/helpers.c.i

src/helpers.s: src/helpers.c.s
.PHONY : src/helpers.s

# target to generate assembly for a file
src/helpers.c.s:
	$(MAKE) $(MAKESILENT) -f CMakeFiles/ncsh.dir/build.make CMakeFiles/ncsh.dir/src/helpers.c.s
.PHONY : src/helpers.c.s

src/io.o: src/io.c.o
.PHONY : src/io.o

# target to build an object file
src/io.c.o:
	$(MAKE) $(MAKESILENT) -f CMakeFiles/ncsh.dir/build.make CMakeFiles/ncsh.dir/src/io.c.o
.PHONY : src/io.c.o

src/io.i: src/io.c.i
.PHONY : src/io.i

# target to preprocess a source file
src/io.c.i:
	$(MAKE) $(MAKESILENT) -f CMakeFiles/ncsh.dir/build.make CMakeFiles/ncsh.dir/src/io.c.i
.PHONY : src/io.c.i

src/io.s: src/io.c.s
.PHONY : src/io.s

# target to generate assembly for a file
src/io.c.s:
	$(MAKE) $(MAKESILENT) -f CMakeFiles/ncsh.dir/build.make CMakeFiles/ncsh.dir/src/io.c.s
.PHONY : src/io.c.s

src/ncsh.o: src/ncsh.c.o
.PHONY : src/ncsh.o

# target to build an object file
src/ncsh.c.o:
	$(MAKE) $(MAKESILENT) -f CMakeFiles/ncsh.dir/build.make CMakeFiles/ncsh.dir/src/ncsh.c.o
.PHONY : src/ncsh.c.o

src/ncsh.i: src/ncsh.c.i
.PHONY : src/ncsh.i

# target to preprocess a source file
src/ncsh.c.i:
	$(MAKE) $(MAKESILENT) -f CMakeFiles/ncsh.dir/build.make CMakeFiles/ncsh.dir/src/ncsh.c.i
.PHONY : src/ncsh.c.i

src/ncsh.s: src/ncsh.c.s
.PHONY : src/ncsh.s

# target to generate assembly for a file
src/ncsh.c.s:
	$(MAKE) $(MAKESILENT) -f CMakeFiles/ncsh.dir/build.make CMakeFiles/ncsh.dir/src/ncsh.c.s
.PHONY : src/ncsh.c.s

# Help Target
help:
	@echo "The following are some of the valid targets for this Makefile:"
	@echo "... all (the default if no target is provided)"
	@echo "... clean"
	@echo "... depend"
	@echo "... edit_cache"
	@echo "... rebuild_cache"
	@echo "... ncsh"
	@echo "... src/cmds.o"
	@echo "... src/cmds.i"
	@echo "... src/cmds.s"
	@echo "... src/headers.o"
	@echo "... src/headers.i"
	@echo "... src/headers.s"
	@echo "... src/helpers.o"
	@echo "... src/helpers.i"
	@echo "... src/helpers.s"
	@echo "... src/io.o"
	@echo "... src/io.i"
	@echo "... src/io.s"
	@echo "... src/ncsh.o"
	@echo "... src/ncsh.i"
	@echo "... src/ncsh.s"
.PHONY : help



#=============================================================================
# Special targets to cleanup operation of make.

# Special rule to run CMake to check the build system integrity.
# No rule that depends on this can have commands that come from listfiles
# because they might be regenerated.
cmake_check_build_system:
	$(CMAKE_COMMAND) -S$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR) --check-build-system CMakeFiles/Makefile.cmake 0
.PHONY : cmake_check_build_system

