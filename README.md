# ncsh

Have you ever thought, "Man, I enjoy C's syntax so much, I wish I could use it in a shell."

Well now you can!* Use ncsh (Neo C Shell) to run C code directly from your shell and enjoy C-like performance!

<sub> * - sort of </sub>

## Building

- Requires [editline](https://troglobit.com/projects/editline/) to compile!
- `make build` to build 
- `make run` to run

My building environment:
- GCC version 12.2.1 (glibc)
- GNU Make 4.4
- An x86_64 Processor

## Features

- C-like performance (blazingly fast)
- The entirety of the C standard library (maybe)

<sub> Note: If you wish to use Valgrind and use Clang to compile, I have experienced issues in the past </sub>

## Contributing

So you want to contribute? Awesome! Just be sure to read over the following:

### Only Submitting an Issue?

If you're only opening an issue, make sure it's not already in the [Known Issues](https://git.chrisfore.us/csfore/ncsh/src/branch/main/KNOWN_ISSUES.md) file.

### Submitting a Pull Request?

- Fork the repository (Can be on GitLab or Forgejo, I'll handle any issues with parity)
- After you clone, checkout a new branch with `git checkout -b new_feature`
- Push your code
- Submit a PR!

## License

This code is licensed under the MIT license.