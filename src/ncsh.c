#include "ncsh.h"

#include <stdlib.h>
#include <string.h>
#include <editline.h>
#include <sys/stat.h>
#include <unistd.h>
#include "helpers.h"
#include "io.h"
#include "cmds.h"

#define PATH_MAX 4096

int check_file(char* path)
{
	struct stat buffer;
	int         status;
	status = stat(path, &buffer);
	return status;
}

// char* check_path(char** path_arr, int path_size, char* line) {

// }

int run_path(char** path_arr, char* line, int size)
{
	// printf("%d\n", path_size);
	// int pid = fork();
	// char **line_cpy = malloc(strlen(line) + 1);
	char *str1, *str2, *token, *subtoken;
    char *saveptr1, *saveptr2;
	char **args = malloc(100 * sizeof(line));
    int j;
	// strcpy(line_cpy, line)
	// char delim[] = " ";
	// printf("%s\n", args);

	// int num_args = 0;
	// while (args != NULL) {
	// 	args = strtok(NULL, delim);
	// 	num_args++;
	// }
	// char *const *args = {NULL};
	
	for (j = 0, str1 = line; ; j++, str1 = NULL) {
        token = strtok_r(str1, " ", &saveptr1);
		args[j] = token;
        if (token == NULL) {
            break;
		}
        // printf("%d: %s\n", j, token);
    }
	// printf("Args: %s\n", saveptr1);
	// for (int i = 0; i < sizeof(args); i++) {
	// 	printf("%s\n", args[i]);
	// }
	// execvp(args[0], args);

	int n;
	for (int i = 0; i < size; i++) {
		n = fork();
		if (n == 0) {
			char *fmt = (char *) malloc(100 * sizeof(char));
			// // for (int i = 0; i < num_args; i++) {
			// // 	printf("%s\n", args);
			// // }
			snprintf(fmt, 128,"%s/%s", path_arr[i], args[0]);
			// printf("%s\n", fmt);
			// // if (pid == 0) {
			execvp(fmt, NULL);
			exit(EXIT_SUCCESS);
		}
	}
		
		// printf("%d\n", res);
	// return 0;
}

// ncsh
int main()
{
	// signed int result = system("asdfg");
	// printf("%d\n", result);
	char* PATH = getenv("PATH");
	// printf("PATH: %s\n", PATH);
	char **PATH_ARR = malloc(PATH_MAX * sizeof(char*));

	char *path_entry = strtok(PATH, ":");
	int size = 0;

	while (path_entry != NULL) {
		PATH_ARR[size++] = path_entry;
		path_entry = strtok(NULL, ":");
	}

	// for (int i = 0; i < size; i++) {
	// 	printf("%s\n", PATH_ARR[i]);
	// }

	// for (int i = 0; i < sizeof(PATH_ARR)/sizeof(PATH_ARR[0]); i++) {
	// 	printf("%s\n", PATH_ARR[i]);
	// }

	check_ncsh_dir();
	/*
	 * Setup Process
	*/

	Shell shell;

	Headers headers;

	char* line;

	char* mode_s = getenv("NCSH_MODE");
	if (mode_s == NULL) {
		printf("Environment Variable `NCSH_MODE` not set, defaulting to 0. Run `.help` to find out more.\n");
		setenv("NCSH_MODE", "0", 0);
		mode_s = getenv("NCSH_MODE");
	}

	char* path = getenv("PATH");

	// Preparing mode
	int mode = strtol(mode_s, NULL, 0);
	if (mode != 0 && mode != 1) {
		fprintf(stderr, "Invalid mode detected, setting to `0`\n");
		mode = 0;
	}

	// Setting up the file to open
	shell.file = fopen("/tmp/ncsh/ncsh_out.c", "w");
	
	if (shell.file == NULL) {
		printf("An error occurred! (Permission to read/write maybe?)\n");
		exit(1);
	}

	// Setting up the file skeleton
	file_setup(shell.file);
	
	char* path_split = strtok(path, ":");

	char** path_arr = malloc(1 * sizeof(*path_arr));

	int path_size = 0;
	while (path_split != NULL) {
		char** tmp = realloc(path_arr, (path_size+1) * sizeof(*path_arr));
		// printf("%s\n", *tmp);
		if (tmp) {
			path_arr = tmp;
			path_arr[path_size] = path_split;
			path_split = strtok(NULL, ":");
			path_size++;
			// printf("%d\n", path_size);
		}
	}

	// for (int i = 0; i < path_size; i++) {
	// 	// printf("eheeee\n");
	// 	printf("%s\n", path_arr[i]);
	// }
	// printf("%d\n", check_file("/home/csfore/Workspaces/c/ncsh/LICENSE"));

	/*
	 * Where most of the action takes place
	*/
	// char* argument_list[] = {"-l", NULL};
	// int ret = execvp("ls", argument_list);

	while (strcmp((line = readline("> ")), ".exit") != 0) {
		initHeaders(&headers, 1);
		shell.headers = headers;
		shell.mode = mode;
		shell.cmd = line;

		// void ret = execvp("/bin/sh", "sh", "-c", command, (char *) NULL);
		// printf("%d\n", run_path(path_arr, path_size, line));

		// char* exec = strcat("/bin/sh -c ", line);
		// system("/usr/bin/env sh -c ls");
		// execl("/bin/sh", "sh", "-c", "ls", (char *) NULL);
		// printf("%s\n", exec);
		// printf("%s\n", line);
		// system(line);
		// printf("%d\n", run_path(path_arr, path_size, &line))
		int res = run_path(PATH_ARR, line, size);
		printf("%d\n", res);

		if (res == 0) {
			printf("Success.\n");
			free(line);
			continue;
		}
		// for (int i = 0; i < path_size; i++) {
		// 	// printf("eheeee\n");
		// 	char* file = strcat(path_arr[i], line);
		// 	printf("%s\n", file);
		// 	if (check_file(file) == 0) {
		// 		system(file);

		// 	}
		// }

		// Handling commands
		if (line[0] == '.') {
			run_cmd(&shell);
			free(line);
			continue;
		}

		if (strcmp(line, "") == 0) {     // If nothing is entered
			free(line);
			continue;
		}



		check_and_output(&shell, mode, line);
	}
	printf("Exiting...\n");
	freeHeaders(&headers);
}