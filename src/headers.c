#include "headers.h"

#include <stdlib.h>

// Initializing the headers list
void initHeaders(Headers* a, size_t initial_size)
{
	a->array = malloc(initial_size * sizeof(char*));
	a->used = 0;
	a->size = initial_size;
}

// Inserting a header
void insertHeader(Headers* a, char* element)
{
	if (a->used == a->size) {
		a->size *= 2;
		a->array = realloc(a->array, a->size * sizeof(char*));
	}
	a->array[a->used++] = element;
}

// Freeing the headers
void freeHeaders(Headers* a)
{
	free(a->array);
	a->array = NULL;
	a->used = a->size = 0;
}