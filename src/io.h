#ifndef IO_H
#define IO_H

#include <stdio.h>

void file_setup(FILE*);
void file_end(FILE*);
void check_ncsh_dir(void);
void file_copy(char*);
void cmd_add(FILE*, char*);
void remove_tmp_lines(void);
int get_lines(FILE*);
void print_lines(FILE*);

#endif