#include "helpers.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "io.h"

// Run the output elf
void run()
{
	system("cc /tmp/ncsh/ncsh_out.c -o /tmp/ncsh/ncsh_out");
	system("/tmp/ncsh/ncsh_out");
}

bool is_preproc(char* directive)
{
	char directives[2][9] = {"#include", "#define"};
	for (int i = 0; i < sizeof(directives) / sizeof(directives[0]); i++)
		if (strstr(directive, directives[i]) != NULL)
			return true;
	return false;
}

void check_and_output(Shell* shell, int mode, char* line)
{
	if (mode == 0) {
		fprintf(shell->file, "\t%s", line);
		file_end(shell->file);
		fclose(shell->file);
		run();
		free(line);
		shell->file = fopen("/tmp/ncsh/ncsh_out.c", "w");
		file_setup(shell->file);
	} else {
		if (is_preproc(line)) {
			insertHeader(&shell->headers, line);
			fclose(shell->file);
			file_copy(line);
			shell->file = fopen("/tmp/ncsh/ncsh_out.c", "a");
		} else
			fprintf(shell->file, "\t%s", line);
		free(line);
	}
}