#ifndef HEADERS_H
#define HEADERS_H

#include <stddef.h>

typedef struct {
	char** array;
	size_t used;
	size_t size;
} Headers;

void initHeaders(Headers*, size_t);
void insertHeader(Headers*, char*);
void freeHeaders(Headers*);

#endif