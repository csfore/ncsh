#ifndef NCSH_H
#define NCSH_H

#include <stdio.h>
#include "headers.h"

typedef struct {
	char*   cmd;
	int     mode;
	FILE*   file;
	Headers headers;
} Shell;

#endif