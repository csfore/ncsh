#include "cmds.h"

#include <stdlib.h>
#include <string.h>
#include "io.h"

// Help command
void help()
{
	printf("ncsh internal commands\n");
	printf(".help - prints this menu\n");
	printf(".run  - runs the code\n");
	printf(".exit - exits\n");
	printf("\nEnvironment Variables:\n");
	printf("NCSH_MODE\n");
	printf("  0 - Automatically run\n");
	printf("  1 - Manually run\n");
}

void get_mode(int mode)
{
	char* name_mode = (mode == 0) ? "Automatic" : "Manual";
	printf("Current Mode: %d (%s)\n", mode, name_mode);
}

void get_headers(Headers headers) {
	printf("Headers added: (%zu)\n", headers.used);
	for (int i = 0; i < headers.used; i++)
		printf("- %s\n", headers.array[i]);
}

void run_program(FILE* file)
{
	file_end(file);
	fclose(file);
	system("cc /tmp/ncsh/ncsh_out.c -o /tmp/ncsh/ncsh_out");
	system("/tmp/ncsh/ncsh_out");
	remove_tmp_lines();
}

void run_cmd(Shell* shell)
{
	if (strcmp(shell->cmd, ".help") == 0)
		help();
	else if (strcmp(shell->cmd, ".mode") == 0)
		get_mode(shell->mode);
	else if (strcmp(shell->cmd, ".run") == 0) {
		if (shell->mode == 1) {
			run_program(shell->file);
			shell->file = fopen("/tmp/ncsh/ncsh_out.c", "a");
		}
		else
			printf("Manually executing is unavailable while in REPL mode.\n");
	} else if (strcmp(shell->cmd, ".headers") == 0)
		get_headers(shell->headers);
	else
		printf("Invalid Command.\n");
}