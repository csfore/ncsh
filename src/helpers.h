#ifndef HELPERS_H
#define HELPERS_H

#include "ncsh.h"
#include <stdbool.h>

void run(void);
bool is_preproc(char*);
void check_and_output(Shell*, int, char*);

#endif