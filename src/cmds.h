#ifndef CMDS_H
#define CMDS_H

#include "ncsh.h"

void help(void);
void run_cmd(Shell*);
void run_program(FILE*);

#endif