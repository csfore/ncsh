#include "io.h"

#include <sys/stat.h>
#include <string.h>

// Struct for making sure the tmp dir exists
struct stat st = {0};

// Adding the skeleton for the file
void file_setup(FILE* file)
{
	fprintf(file, "#include <stdio.h>\n");
	fprintf(file, "\n");
	fprintf(file, "void ncsh() {\n");
}

void cmd_add(FILE* file, char* cmd)
{
	fprintf(file, "\t%s\n", cmd);
}

// Completing the file
void file_end(FILE* file)
{
	fprintf(file, "\n}\n");
	fprintf(file, "int main() {\n");
	fprintf(file, "\tncsh();\n");
	fprintf(file, "\treturn 0;\n");
	fprintf(file, "}");
}

// Checking for the tmp dir
void check_ncsh_dir()
{
	if (stat("/tmp/ncsh", &st) == -1)
		mkdir("/tmp/ncsh", 0777);
}

void remove_tmp_lines()
{
	char* line      = NULL;
	size_t len      = 0;
	int i           = 0;

	FILE* old = fopen("/tmp/ncsh/ncsh_out.c", "r");
	FILE* new = fopen("/tmp/ncsh/ncsh_out.c.tmp", "w");

	while (getline(&line, &len, old) != -1) {
		char* main_check = strstr(line, "}\n"); 
		if (main_check != NULL)
			break;
		fprintf(new, "%s", line);
		i++;
	}
	fclose(old);
	fclose(new);
	rename("/tmp/ncsh/ncsh_out.c.tmp", "/tmp/ncsh/ncsh_out.c");
}

// Copying the file and adding in headers
void file_copy(char* header)
{
	char* line      = NULL;
	size_t len      = 0;
	int i           = 0;

	FILE* original = fopen("/tmp/ncsh/ncsh_out.c", "r");
	FILE* new = fopen("/tmp/ncsh/ncsh_out.c.tmp", "w");
	while (getline(&line, &len, original) != -1) {
		if (i == 1) {
			fprintf(new, "%s\n", header);
			i++;
			continue;
		}
		fprintf(new, "%s", line);
		i++;
	}
	fclose(new);
	rename("/tmp/ncsh/ncsh_out.c.tmp", "/tmp/ncsh/ncsh_out.c");
}