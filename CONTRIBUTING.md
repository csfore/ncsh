# Contributing

## Style Guide

For the most part I try to follow the Linux Kernel [coding style](https://www.kernel.org/doc/html/v6.1/process/coding-style.html#the-inline-disease), obviously it won't be 100% accurate because of habits.